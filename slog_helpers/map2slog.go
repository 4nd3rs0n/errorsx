package slog_helpers

import (
	"log/slog"
)

// MapToSlog converts map of anything with string key to a loggable by log/slog slice
//
// Usage:
//
//	log := MapToSlog(map)
//	log = append(log, slog.Group("Map2", MapToSlog(map2)...))
//	slog.Info("Some information", log...)
func MapToSlog[T any](in map[string]T) []any {
	var log []any
	for key, val := range in {
		log = append(log, slog.Any(key, val))
	}
	return log
}
