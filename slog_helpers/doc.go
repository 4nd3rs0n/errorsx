// Package slog_helpers provides functionality to transform some golang types into a loggable by the package log/slog.
// As all this module oriented to improve error handling, I think it's right place for this package.
// Also, those helpers are used inside the project in domain_errors.
package slog_helpers
