package slog_helpers_test

import (
	"encoding/json"
	"io"
	"log/slog"
	"net/http"
	_ "net/http"
	"net/url"
	"os"
	"testing"

	"gitlab.com/4nd3rs0n/errorsx/slog_helpers"
)

type TestBody struct {
	io.ReadCloser
	data   []byte
	offset int
	closed bool
}

func (tb *TestBody) Read(p []byte) (n int, err error) {
	if tb.offset >= len(tb.data) {
		return 0, io.EOF
	}

	n = copy(p, tb.data[tb.offset:])
	tb.offset += n
	return n, nil
}

func (tb *TestBody) Close() error {
	tb.closed = true
	return nil
}

func (tb *TestBody) IsClosed() bool {
	return tb.closed
}

func NewTestBody(data []byte) TestBody {
	return TestBody{data: data}
}

type BodyType struct {
	Something     string `json:"something"`
	SomethingElse int    `json:"somethingElse"`
}

// ================= //
// Tests starts here //
// ================= //

func TestHttpReqToSlog(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	}))

	bodyBytes, _ := json.Marshal(BodyType{
		Something:     "ABCD",
		SomethingElse: 12345,
	})
	body := NewTestBody(bodyBytes)

	t.Run("simple", func(t *testing.T) {
		req := &http.Request{
			Method: http.MethodGet,
			Host:   "0.0.0.0",
			Header: http.Header{},
			URL: &url.URL{
				Path: "/abc",
			},
			Body: &body,
		}
		req.Header.Add("TEST", "TEST")
		req.Header.Add("TEST2", "TEST2")

		// I'm too lazy to write some code to capture the output with io.Writer,
		// than parse it, than check values in output... Meh. It just works, see?
		// But if it don't for you -- you haven't seen this code :)
		//
		// You may contribute into tests, if you really want to
		attrs, err := slog_helpers.HttpReqToSlog(req, slog_helpers.RequestToSlogCfg{
			LogURL:     true,
			LogHeaders: true,
			LogBody:    true,
		})
		if err != nil {
			t.Error(err)
		}
		if !body.IsClosed() {
			t.Error("Body haven't been closed!")
		}
		logger.Info("SimpleTest", attrs...)
	})
}
