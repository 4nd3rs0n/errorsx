package slog_helpers

import (
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"strings"
)

type RequestToSlogCfg struct {
	LogURL     bool
	LogHeaders bool
	LogBody    bool
}

func getFullURL(r *http.Request) string {
	// Get the scheme/protocol
	protocol := "http"
	if r.TLS != nil {
		protocol = "https"
	}

	return fmt.Sprintf("%s://%s%s%s%s",
		protocol,
		r.Host,
		r.URL.Path,
		r.URL.RawQuery,
		r.URL.Fragment)
}

// HttpReqToSlog converts an HTTP request (URL, Headers, Body) to the loggable by slog format
// TODO: Add IP logging
func HttpReqToSlog(r *http.Request, cfg RequestToSlogCfg) ([]any, error) {
	var attrs []any

	if cfg.LogURL {
		attrs = append(attrs, slog.String("method", r.Method))
		attrs = append(attrs, slog.String("url", getFullURL(r)))
	}

	if cfg.LogHeaders {
		var headersGroup []any
		for key, val := range r.Header {
			headersGroup = append(headersGroup,
				slog.String(key, strings.Join(val, ", ")))
		}

		attrs = append(attrs, slog.Group("headers", headersGroup...))
	}

	if cfg.LogBody {
		body, err := io.ReadAll(r.Body)
		if err != nil {
			return []any{}, err
		}
		err = r.Body.Close()
		if err != nil {
			return []any{}, err
		}
		attrs = append(attrs, slog.String("body", string(body)))
	}

	return attrs, nil
}
