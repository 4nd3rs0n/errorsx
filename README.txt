Golang error implementation with an additional information

It can be useful if you need to provide information to error, like place of an error 
(for example, to choose which HTTP code to return based on the error place: validation, business logic, repository), 
states for structured logging and any other information that you may need to be handled with error

Minimal Go version to use: 1.18
