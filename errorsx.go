package errorsx

type CustomErr[T any] interface {
	// Error is a method that returns error text
	Error() string
	// Info is a method that returning additional info for error
	Info() T
}

// NewCustomErr is a custom error constructor
// Usage:
//
//	type ErrInfo struct {
//		Critical bool
//	}
//	errx := errorsx.NewCustomErr("Message", ErrInfo{
//		Critical: false,
//	})
//	if errx != nil {
//		errInf := errx.Info()
//		fmt.Println("Error:", errx.Error())
//		fmt.Println("Critical:", errInf.Critical)
//	}
func NewCustomErr[T any](err string, additionalInf T) CustomErr[T] {
	customError := &customErr[T]{
		err:  err,
		ainf: additionalInf,
	}
	return customError
}

type customErr[T any] struct {
	err  string
	ainf T
}

func (err *customErr[T]) Error() string {
	return err.err
}
func (err *customErr[T]) Info() T {
	return err.ainf
}
