package varserr

import (
	"log/slog"

	"gitlab.com/4nd3rs0n/errorsx"
	slogHelpers "gitlab.com/4nd3rs0n/errorsx/slog_helpers"
)

type VarsErrInfo map[string]any

// VarsErr is a type of error that allows to put a map of any states (variables) as
// an additional info to your errors. That can be useful for structured logging.
//
// This package provides LogVarsErr function that allows to easily log that type of
// errors using slog
type VarsErr errorsx.CustomErr[VarsErrInfo]

func NewVarsErr(err string, vars map[string]any) errorsx.CustomErr[VarsErrInfo] {
	return errorsx.NewCustomErr[VarsErrInfo](err, vars)
}

func LogVarsErr(vErr VarsErr, logger *slog.Logger) {
	var log []any = slogHelpers.MapToSlog(vErr.Info())
	logger.Error(vErr.Error(), log...)
}
