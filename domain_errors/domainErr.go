package domain_errors

import (
	"fmt"
	"log/slog"
	"net/http"

	"gitlab.com/4nd3rs0n/errorsx"
	slogHelpers "gitlab.com/4nd3rs0n/errorsx/slog_helpers"
)

type DomainErrInfo struct {
	Log        bool
	HttpCode   int
	ApiMessage string
	Vars       map[string]any
}

// DomainErr is an error that is great for structured logging due it's ability to store any map of data
// and it contains information about how to handle it
// What makes it a perfect choise for using on the domain layer of the application
// That error can be easily handled using LogDomainErr and HttpHandleDomainErr functions
type DomainErr errorsx.CustomErr[DomainErrInfo]

// NewDomainErr create an error that is great for structured logging and contains information
// about how to handle it (log it or not, how handle this error on API layer)
func NewDomainErr(err string, inf DomainErrInfo) DomainErr {
	return errorsx.NewCustomErr(err, inf)
}

// Just logs error if err.Info().Log is true
// It don't check if logger or error is nil. Check it before using this function in your code.
// Overwise this function will panic
func LogDomainErr(
	dErr DomainErr,
	logger *slog.Logger) {

	einf := dErr.Info()
	if einf.Log {
		var log []any = slogHelpers.MapToSlog(einf.Vars)
		logger.Error(dErr.Error(), log...)
	}
}

// HttpHandleDomainErr handles domain error for you in http handler.
// Just pass values and it sets http body, http code and logs the error if err.Info().Log is true.
//
// This function don't check if logger or error or request is nil.
// Check it before using this function in your code. Overwise you'll get a panic
//
// Usage:
//
//		...
//		// Somewhere inside the http handler
//		someData, errx := domain.SomeLogic( ... )
//		if errx != nil {
//		  HttpHandleDomainErr(errx, logger, w, r)
//	    return
//		}
func HttpHandleDomainErr(
	dErr DomainErr,
	logger *slog.Logger,
	w http.ResponseWriter,
	r *http.Request) {

	einf := dErr.Info()

	var logBody bool
	if r.Method == http.MethodPost || r.Method == http.MethodPatch {
		logBody = true
	}

	var apiMsg string = dErr.Error()
	if einf.ApiMessage != "" {
		apiMsg = einf.ApiMessage
	}

	var httpCode int = 500
	if einf.HttpCode > 0 {
		httpCode = einf.HttpCode
	}

	w.WriteHeader(httpCode)
	w.Write([]byte(apiMsg))

	if einf.Log {
		var log []any = slogHelpers.MapToSlog(einf.Vars)
		reqLog, err := slogHelpers.HttpReqToSlog(r, slogHelpers.RequestToSlogCfg{
			LogURL:     true,
			LogHeaders: true,
			LogBody:    logBody,
		})
		if err != nil {
			panic(fmt.Errorf("Failed to convert request to a slog. Error: %v\n", err))
		} else {
			log = append(log, slog.Group("request", reqLog...))
		}
		logger.Error(dErr.Error(), log...)
	}
}
