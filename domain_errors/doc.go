// Package domain_errors provides errors that I use in my applications on the domain layer. Those errors are easy
// to handle and use on the API layer. Those errors have information about HTTP code, log it or not, api message, any
// additional states. Also, this package provides some
package domain_errors
